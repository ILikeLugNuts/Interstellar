const config = {
  challenge: true,
  users: {
    // username: 'password', you can add multiple users.
    interstellar: 'passwords-are-amazing',
    brandon: 'rupert_the_great',
    ilikelugnuts: 'imhorrorjaxon',
    austin: 'urwelcome',
    owen: 'frfrfr',
  },
}
export default config
